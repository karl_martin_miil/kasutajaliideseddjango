//vrtuaalsesse keskonda saamine
virtualenv nimi
./bin/activate

django-admin startproject projektinimi

cd projektinimi
./manage.py startapp appname

./manage.py managemigrations

./manage.py migrate

./manage.py createsuperuser

./manage.py runserver 0.0.0.0:8000
//change port